<?php

namespace Bolt\Extension\ProjektDigital\AllegroSearch\Controller;

use Twig\Markup;
use Silex\Application;
use Bolt\Controller\Base;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Bolt\Extension\ProjektDigital\AllegroSearch\AllegroSearchExtension;

/**
 * The controller for Sitemap routes.
 */

class AllegroSearch extends Base
{
    /** @var Application */
    protected $app;

    /**
     * {@inheritdoc}
     */
    public function addRoutes(ControllerCollection $c)
    {
        $c->match('/allegrosearch', [$this, 'allegrosearch']);

        return $c;
    }

    public function connect(Application $app)
    {
        $this->app = $app;

        /** @var ControllerCollection $ctr */
        $ctr = $app['controllers_factory'];

        // This matches both GET requests.
        $ctr->match('allegrosearch', [$this, 'allegroSearch'])
            ->bind('allegrosearch')
            ->method('GET');

        $ctr->match('postallegrosearch', [$this, 'postAllegroSearch'])
            ->bind('postAllegroSearch')
            ->method('POST');

        return $ctr;
    }

    public function postAllegroSearch(Application $app, Request $request)
    {
        $ext = new AllegroSearchExtension();
        $records = $ext->postExtLibrarySearch($app, $request);

        $twig = $app['twig'];
        $html = $twig->render('searchresult.twig', array('records' => $records, 'sql' => $ext->sql, 'maxrelevance' => $ext->maxRelevance));
        return new Markup($html, 'UTF-8');
    }

    public function allegroSearch(Application $app, Request $request)
    {
        $twig = $app['twig'];

        $html = $twig->render('searchpage.twig', array());
        return new Markup($html, 'UTF-8');
    }
}
