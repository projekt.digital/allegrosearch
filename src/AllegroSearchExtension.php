<?php

namespace Bolt\Extension\ProjektDigital\AllegroSearch;

use Silex\Application;
use Twig\Markup;
use Bolt\Asset\File\JavaScript;
use Silex\ControllerCollection;
use Bolt\Extension\SimpleExtension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AllegroSearchExtension extends SimpleExtension
{
    private $freeSearchArray = [
        'title', '_00', '_20', '_23', '_28', '_30a', 
        '_31', '_40', '_40a', '_40b', '_40c', '_41', 
        '_42', '_50', '_50f', '_60', '_61', '_71', '_74',
        '_75', '_76', '_76p', '_77', '_80', '_84', '_85',
        '_87', '_88', '_90', 'slug'
    ];

    private $titleSearchArray = [
        'title', '_00', '_20', '_23', '_80', '_84', '_85'
    ];

    private $authorSearchArray = [
        '_40', '_40a', '_40b', '_40c', '_41', '_42', '_50', 
        '_50f', '_60', '_61'
    ];

    private $sloganSearchArray = ['_28', '_31'];

    private $exactTitleSearchArray = ['_20'];

    private $signatureSearchArray = ['_90'];

    private $yearSearchArray = ['_76'];

    private $placePublisherSearchArray = ['_60', '_61', '_74', '_75'];

    private $isbnSearchArray = ['_87', '_88'];

    protected function registerServices(Application $app)
    {
        $app['allegrosearch.controller'] = $app->share(
            function () {
                return new Controller\AllegroSearch();
            }
        );
    }

    protected function registerFrontendControllers()
    {
        $app = $this->getContainer();

        return [
            '/' => $app['allegrosearch.controller'],
        ];
    }

    protected function registerTwigPaths()
    {
        return [
            'templates',
        ];
    }

    public $sql = "";
    public $maxRelevance = 1;

    public function postExtLibrarySearch(Application $app, Request $request)
    {
        // main search 1
        $searchType1 = $request->request->get('searchtype');
        $search1 = $request->request->get('search');
        // $search1 = str_replace(':', '\:', $search1);

        // main search 2
        $selectType2 = $request->request->get('addsearch1');
        $searchType2 = $request->request->get('searchtype1');
        $search2 = $request->request->get('search1');
        // $search2 = str_replace(':', '\:', $search2);

        // // main search 3
        $selectType3 = $request->request->get('addsearch2');
        $searchType3 = $request->request->get('searchtype2');
        $search3 = $request->request->get('search2');

        // sort order / sort by
        $sortBy = $request->request->get('sortby');
        $sortOrder = $request->request->get('sortorder');

        // publication years
        $pubYear1 = $request->request->get('pub01');
        $pubYear2 = $request->request->get('pub02');

        if (! ($search1 || ($pubYear1 && $pubYear2))) {
            $app['session']->getFlashBag()->set('info', 'Sorry, enter search parameter!');
            return new RedirectResponse('/librarysearch');
        }

        // get content result
        $entries = $app['storage']->getRepository('libraryentries');
        $qb = $entries->createQueryBuilder();

        // main search
        // 2 is equal to 'or'
        if ($search1) {
            $qb = $this->dbQueryOnSearchType($searchType1, $search1, $qb, '2');
        }

        // search 2
        if ($search2) {
            $qb = $this->dbQueryOnSearchType($searchType2, $search2, $qb, $selectType2);
        }

        // search 3
        if ($search3) {
            $qb = $this->dbQueryOnSearchType($searchType3, $search3, $qb, $selectType3);
        }

        if ($pubYear1 && $pubYear2) {
            $yearCol = 'CAST(_76 AS REAL)';
            $newQuery = $qb->expr()->andX();

            $newQuery->add("{$yearCol} >= CAST('{$pubYear1}' AS REAL)");
            $newQuery->add("{$yearCol} <= CAST('{$pubYear2}' AS REAL)");

            $qb->andWhere($newQuery);
        }
        elseif ($pubYear1) {
            $yearCol = 'CAST(_76 AS REAL)';
            $qb->andWhere("{$yearCol} >= CAST('{$pubYear1}' AS REAL)");
        }
        elseif ($pubYear2) {
            $yearCol = 'CAST(_76 AS REAL)';
            $qb->andWhere("{$yearCol} <= CAST('{$pubYear2}' AS REAL)");
        }

        if ($sortOrder === '1') {
            $sortOrder = 'ASC';
        } else {
            $sortOrder = 'DESC';
        }

        $sortByRev = false;

        if ($sortBy === '2') {
            $sortBy = 'title';
        } elseif ($sortBy === '3') {
            $sortBy = '_76';
        } else {
            $sortByRev = true;
        }

        if (! $sortByRev) {
            $qb->orderBy($sortBy, $sortOrder)->setMaxResults(150);
        }

        $this->sql = $qb->getSQL();
        $records = $qb->execute()->fetchAll();

        // determine relevance
        $max_relevance = 0;
        for ($i = 0; $i < count($records); $i++) {
            $relevance = 0;
            if (!empty($search1)) $relevance += $this->getRelevance($searchType1, $search1, $records[$i]);
            if (!empty($search2)) $relevance += $this->getRelevance($searchType2, $search2, $records[$i]);
            if (!empty($search3)) $relevance += $this->getRelevance($searchType3, $search3, $records[$i]);
            $records[$i]['relevance'] = $relevance;
            if ($relevance > $max_relevance) $max_relevance = $relevance;
        }
        $this->maxRelevance = $max_relevance;

        if ($sortByRev) {
            usort($records,function($a,$b){
                return $b['relevance'] - $a['relevance'];
            });
        }

        return $records;
    }

    public function dbQueryOnSearchType($searchType, $query, $qb, $selectType)
    {
        // $words = explode(' ', $query);
        $words = array_filter(array_map('trim', explode(' ', $query)));

        if ($searchType === '1') {
            return $this->searchFunc($words, $qb, 'freeSearchArray', false, $selectType);
        } else if ($searchType === '2') {
            return $this->searchFunc($words, $qb, 'titleSearchArray', false, $selectType);
        } else if ($searchType === '3') {
            return $this->searchFunc($words, $qb, 'authorSearchArray', false, $selectType);
        } else if ($searchType === '4') {
            return $this->searchFunc($words, $qb, 'sloganSearchArray', false, $selectType);
        } else if ($searchType === '5') {
            // send array length of 1 with complete search input
            return $this->searchFunc([implode(" ", $words)], $qb, 'exactTitleSearchArray', true, $selectType);
        } else if ($searchType === '6') {
            return $this->searchFunc($words, $qb, 'signatureSearchArray', false, $selectType);
        } else if ($searchType === '7') {
            return $this->searchFunc($words, $qb, 'yearSearchArray', false, $selectType);
        } else if ($searchType === '8') {
            return $this->searchFunc($words, $qb, 'placePublisherSearchArray', false, $selectType);
        } else if ($searchType === '9') {
            // send array length of 1 with complete search input
            return $this->searchFunc([implode(" ", $words)], $qb, 'isbnSearchArray', true, $selectType);
        }
    }

    protected function getRelevance($searchType, $query, $record)
    {
        $words = array_filter(array_map('trim', explode(' ', $query)));
        $searchFields = [];

        switch (intval($searchType))
        {
            case '1': $searchFields = $this->freeSearchArray; break;
            case '2': $searchFields = $this->titleSearchArray; break;
            case '3': $searchFields = $this->authorSearchArray; break;
            case '4': $searchFields = $this->sloganSearchArray; break;
            case '5': $searchFields = $this->exactTitleSearchArray; break;
            case '6': $searchFields = $this->signatureSearchArray; break;
            case '7': $searchFields = $this->yearSearchArray; break;
            case '8': $searchFields = $this->placePublisherSearchArray; break;
            case '9': $searchFields = $this->isbnSearchArray; break;
        }

        $relevance = 0;
        foreach ($searchFields as $field)
        {
            $weight = in_array($field, $this->titleSearchArray) ? 2 : 1;
            foreach ($words as $word)
            {
                $relevance += substr_count(strtolower($record[$field]), strtolower($word)) * $weight;
            }
        }
        return $relevance;
    }

    public function searchFunc($words, $qb, $arrayName, $exactMatch, $selectType)
    {
        $newQuery = $qb->expr()->orX();

        foreach ($this->{$arrayName} as $item) {
            foreach ($words as $word) {
                if ($selectType === '3') {
                    if ($exactMatch) {
                        $newQuery->add("{$item} != '{$word}'");
                    } else {
                        $newQuery->add("{$item} NOT LIKE '%{$word}%'");
                    }
                } else {
                    if ($exactMatch) {
                        $newQuery->add("{$item} = '{$word}'");
                    } else {
                        $newQuery->add("{$item} LIKE '%{$word}%'");
                    }
                }
            }
        }

        if ($selectType === '2') {
            $qb->orWhere($newQuery);
        } else {
            $qb->andWhere($newQuery);
        }

        return $qb;
    }
}